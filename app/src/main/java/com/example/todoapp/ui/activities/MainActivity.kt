package com.example.todoapp.ui.activities

import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SwitchCompat
import androidx.core.util.forEach
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.todoapp.R
import com.example.todoapp.ui.lists.TaskListAdapter
import com.example.todoapp.data.TaskDAOSingleton
import com.example.todoapp.model.Task
import com.example.todoapp.ui.dialogs.ConfirmTaskDeleteDialog
import com.google.android.material.floatingactionbutton.FloatingActionButton

class MainActivity : AppCompatActivity() {
    private lateinit var etxtTask : EditText
    private lateinit var switchUrgent : SwitchCompat
    private lateinit var undoneCheck : CheckBox
    private lateinit var rvTaskList : RecyclerView
    private lateinit var taskListAdapter : TaskListAdapter
    private lateinit var fabDelete : FloatingActionButton

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        this.etxtTask = findViewById(R.id.etxtTask)
        this.switchUrgent = findViewById(R.id.switchUrgent)
        this.undoneCheck = findViewById(R.id.undoneCheck)
        this.fabDelete = findViewById(R.id.fabDelete)
        this.rvTaskList = findViewById(R.id.rvTaskList)
        this.rvTaskList.layoutManager = LinearLayoutManager(this)
        this.rvTaskList.setHasFixedSize(true)
        this.taskListAdapter = TaskListAdapter(TaskDAOSingleton.getAllTasks(this))
                                    .setOnCompletedChangeListener{
                                        val pos = TaskDAOSingleton.update(this,it)
                                    }
            .setOnChangeSelectionListener{
                if(it.size() > 0)
                    this.fabDelete.visibility = View.VISIBLE
                else
                    this.fabDelete.visibility = View.GONE
            }
        this.rvTaskList.adapter = taskListAdapter

        this.undoneCheck.setOnCheckedChangeListener{_,isChecked ->
            this.taskListAdapter.filterCompletedTasks(isChecked)
        }
    }

    fun onClickOk (v: View){
        val valueStr : String = this.etxtTask.text.toString()
        if(valueStr.isNotEmpty()){
            val createdTask = Task(valueStr, this.switchUrgent.isChecked)
            val pos = TaskDAOSingleton.add(this, createdTask)
            this.rvTaskList.adapter!!.notifyItemInserted(pos)
            this.rvTaskList.scrollToPosition(pos)
        }else{
            Toast.makeText(this, R.string.error_task_empty, Toast.LENGTH_SHORT).show()
        }
    }

    fun onClickDeleteSelectedTasks(v: View){
        object : ConfirmTaskDeleteDialog(this){
            override fun onConfirm() {
                taskListAdapter.selectedTasks.forEach {_, value ->
                    val pos = TaskDAOSingleton.delete(baseContext, value)
                    taskListAdapter.notifyItemRemoved(pos)
                }
                taskListAdapter.selectedTasks.clear()
                fabDelete.visibility = View.GONE
            }
        }
    }
}