package com.example.todoapp.ui.lists

import android.view.View
import android.widget.CheckBox
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.todoapp.R
import com.example.todoapp.model.Task

class TaskListViewHolder (
    itemView: View,
    private val adapter : TaskListAdapter
) : RecyclerView.ViewHolder(itemView), View.OnLongClickListener {

    private val tagUrgency: View = itemView.findViewById(R.id.tagUrgency)
    private val textTask: TextView = itemView.findViewById(R.id.textTask)
    private val checkboxDone : CheckBox = itemView.findViewById(R.id.checkboxDone)
    private lateinit var task: Task

    init{
        itemView.setOnLongClickListener(this)
        this.checkboxDone.setOnCheckedChangeListener{ _, isChecked ->
            this.task.setCheck(isChecked)
            this.adapter.getOnCompletedChangeListener()?.onCompleteTask(this.task)
        }
    }

    fun bind(task: Task){
        this.task = task
        if(task.getUrgency()){
            this.tagUrgency.setBackgroundColor(0xFFFC6C6C.toInt())
        }   else{
            this.tagUrgency.setBackgroundColor(0xFFCAF994.toInt())
        }
        this.textTask.text = task.getMessage()
        this.checkboxDone.isChecked = task.getCheck()
        itemView.isSelected = this.adapter.selectedTasks.get(this.task.getID().toInt()) != null
    }

    override fun onLongClick(p0: View?): Boolean {
        itemView.isSelected = !itemView.isSelected
        if(this.adapter.selectedTasks.get(this.task.getID().toInt()) != null)
            this.adapter.selectedTasks.delete(this.task.getID().toInt())
        else
            this.adapter.selectedTasks.append(this.task.getID().toInt(), this.task)
        val selectedArray = this.adapter.selectedTasks.clone()
        this.adapter.getOnChangeSelectionListener()?.onChangeSelection(selectedArray)
        return true
    }
}