package com.example.todoapp.ui.dialogs

import android.app.AlertDialog
import android.content.Context
import com.example.todoapp.R

abstract class ConfirmTaskDeleteDialog(
    private val context: Context
) {
    init{
        AlertDialog.Builder(this.context)
            .setTitle(R.string.del_channel_operation)
            .setMessage(R.string.r_u_sure_del_task)
            .setPositiveButton(R.string.yes){_,_ ->
                this.onConfirm()
            }.setNegativeButton(R.string.no, null)
            .create().show()
    }
    abstract fun onConfirm()
}