package com.example.todoapp.ui.lists

import android.util.SparseArray
import android.util.SparseBooleanArray
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.todoapp.R
import com.example.todoapp.model.Task

class TaskListAdapter(
    private var taskList : ArrayList<Task>
): RecyclerView.Adapter<TaskListViewHolder>() {

    private var selectionListener: OnChangeSelectionListener? = null
    private var completeListener : OnCompletedChangeListener? = null
    var selectedTasks : SparseArray<Task> = SparseArray()
    private var tempTaskList = taskList

    fun interface OnCompletedChangeListener{
        fun onCompleteTask(task: Task)
    }

    fun interface OnChangeSelectionListener{
        fun onChangeSelection(selectedTasks: SparseArray<Task>)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TaskListViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val itemView = inflater.inflate(R.layout.view_task, parent, false)
        return TaskListViewHolder(itemView, this)
    }

    override fun onBindViewHolder(holder: TaskListViewHolder, position: Int) {
        holder.bind(this.taskList[position])
    }

    override fun getItemCount(): Int {
        return this.taskList.size
    }

    fun filterCompletedTasks(showUndoneOnly: Boolean){
        if(showUndoneOnly){
            this.taskList = this.taskList.filter{
                !it.getCheck()
            } as ArrayList<Task>
        }else{
            this.taskList = this.tempTaskList
        }
        this.notifyDataSetChanged()
    }

    //----------------------------------------

    fun getOnCompletedChangeListener() : OnCompletedChangeListener?{
        return this.completeListener
    }

    fun setOnCompletedChangeListener(completeListener: OnCompletedChangeListener): TaskListAdapter {
        this.completeListener = completeListener
        return this
    }

    fun getOnChangeSelectionListener() : OnChangeSelectionListener?{
        return this.selectionListener
    }

    fun setOnChangeSelectionListener(selectionListener: OnChangeSelectionListener): TaskListAdapter {
        this.selectionListener = selectionListener
        return this
    }
}