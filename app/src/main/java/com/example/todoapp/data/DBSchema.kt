package com.example.todoapp.data


object DBSchema {
    object TaskTable{
        const val TABLENAME = "tasks"
        const val ID = "t_id"
        const val MESSAGE = "t_message"
        const val URGENCY = "t_urgency"
        const val DONE = "t_done"
        const val TIMESTAMP = "t_timestamp"

        fun getCreateTableQuery(): String{
            return """
            CREATE TABLE IF NOT EXISTS $TABLENAME (
                $ID INTEGER PRIMARY KEY AUTOINCREMENT,
                $MESSAGE TEXT NOT NULL,
                $URGENCY INTEGER DEFAULT FALSE,
                $DONE INTEGER DEFAULT FALSE,
                $TIMESTAMP TEXT DEFAULT CURRENT_TIMESTAMP
            );
            """.trimIndent()
        }
    }
}