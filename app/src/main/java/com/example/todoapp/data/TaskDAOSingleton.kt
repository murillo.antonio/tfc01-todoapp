package com.example.todoapp.data

import android.content.Context
import com.example.todoapp.model.Task

object TaskDAOSingleton {
    private lateinit var taskList: ArrayList<Task>
    private lateinit var dbTask: DBTaskTableManager

    private fun initDAO(context: Context){
        if(!::taskList.isInitialized){
            this.dbTask = DBTaskTableManager(context)
            this.taskList = this.dbTask.getAllTasks()
        }
    }

    fun add(context: Context, t: Task) : Int{
        this.initDAO(context)
        taskList.add(0, t)
        t.setID(this.dbTask.insert(t))
        return 0
    }

    fun delete(context: Context,t: Task) : Int{
        this.initDAO(context)
        val pos = taskList.indexOf(t)
        this.taskList.removeAt(pos)
        this.dbTask.delete(t)
        return pos
    }

    fun update(context: Context,t: Task) : Int{
        this.initDAO(context)
        val pos = taskList.indexOf(t)
        val task = this.taskList[pos]
        task.setCheck(t.getCheck())
        this.dbTask.updateDoneTask(task)
        return pos
    }

    fun getAllTasks(context: Context): ArrayList<Task> {
        this.initDAO(context)
        return this.taskList
    }
}