package com.example.todoapp.data

import android.annotation.SuppressLint
import android.content.ContentValues
import android.content.Context
import com.example.todoapp.model.Task

class DBTaskTableManager(context: Context) {
    companion object{
        const val WHERE_ID = "${DBSchema.TaskTable.ID} = ?"
        val COLS = arrayOf(DBSchema.TaskTable.ID,DBSchema.TaskTable.MESSAGE,DBSchema.TaskTable.URGENCY,DBSchema.TaskTable.DONE)
        const val ORDER = "${DBSchema.TaskTable.TIMESTAMP} DESC"
    }
    private val dbHelper = DBHelper(context)

    fun insert(task: Task) : Long{
        val cv = ContentValues()
        cv.put(DBSchema.TaskTable.MESSAGE,task.getMessage())
        cv.put(DBSchema.TaskTable.URGENCY,task.getUrgency())
        cv.put(DBSchema.TaskTable.DONE,task.getCheck())
        val db = this.dbHelper.writableDatabase
        val id = db.insert(DBSchema.TaskTable.TABLENAME, null, cv)
        db.close()
        return id
    }

    fun delete(task: Task){
        val db = this.dbHelper.writableDatabase
        db.delete(DBSchema.TaskTable.TABLENAME, WHERE_ID, arrayOf(task.getID().toString()))
        db.close()
    }

    fun updateDoneTask(task: Task){
        val db = this.dbHelper.writableDatabase
        val cv = ContentValues()
        cv.put(DBSchema.TaskTable.DONE,task.getCheck())
        db.update(DBSchema.TaskTable.TABLENAME, cv ,WHERE_ID,arrayOf(task.getID().toString()))
        db.close()
    }

    fun getAllTasks(): ArrayList<Task>{
        val tasks : ArrayList<Task> = ArrayList()
        val db = this.dbHelper.readableDatabase
        val cur = db.query(DBSchema.TaskTable.TABLENAME, COLS, null, null, null, null, ORDER)
        while(cur.moveToNext()){
            val id = cur.getLong(cur.getColumnIndexOrThrow(DBSchema.TaskTable.ID))
            val message = cur.getString(cur.getColumnIndexOrThrow(DBSchema.TaskTable.MESSAGE))
            val urgency = cur.getInt(cur.getColumnIndexOrThrow(DBSchema.TaskTable.URGENCY)) == 1
            val done = cur.getInt(cur.getColumnIndexOrThrow(DBSchema.TaskTable.DONE)) == 1
            val task = Task(id,message,urgency,done)
            tasks.add(task)
        }
        db.close()
        return tasks
    }
}