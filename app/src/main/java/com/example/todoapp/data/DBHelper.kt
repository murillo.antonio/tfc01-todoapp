package com.example.todoapp.data

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class DBHelper(context: Context) : SQLiteOpenHelper(context, DBNAME, null, DBVERSION){
    companion object{
        private const val DBNAME = "todoapp.db"
        private const val DBVERSION = 1
    }

    override fun onCreate(db: SQLiteDatabase?) {
        db?.execSQL(DBSchema.TaskTable.getCreateTableQuery())
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        TODO("Not yet implemented")
    }

}